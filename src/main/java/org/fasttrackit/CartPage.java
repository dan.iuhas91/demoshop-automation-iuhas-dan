package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private SelenideElement emptyCartPageElement = $(".text-center");
    private String emptyCartPage;
    private final SelenideElement trashCanButton = $(".fa-trash");
    private final SelenideElement continueShoppingButton = $(".btn-danger");
    private final SelenideElement checkoutButton = $("[href='#/checkout-info']");
    private final SelenideElement yourCartTtile = $(".text-muted");

    public CartPage() {

    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }

    public void clickOnTheTrashCanButton() {
        System.out.println("Click on the trash can button");
        trashCanButton.click();
    }

    public void clickOnTheContinueShoppingButton() {
        System.out.println("Click on the Continue Shopping button");
        continueShoppingButton.click();
    }
    public void clickOnTheCheckoutButton(){
        System.out.println("Click on the Checkout Button");
        checkoutButton.click();
    }
    public String getCartPageTitle(){
        return yourCartTtile.text();
    }
}

