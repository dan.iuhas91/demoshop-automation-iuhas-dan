package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailPage {

    private final SelenideElement cartIcon = $(".fa-cart-plus");
    private final SelenideElement heartIcon = $(".fa-heart.fa-3x");

    public void clickOnTheDetailedPageCartIcon(){
        System.out.println("Click on the Detailed Product Page Cart Icon");
        cartIcon.click();
    }
    public void clickOnTheDetailedProductPageHeartIcon(){
        System.out.println("Click on the Detailed Product Page Heart Icon");
        heartIcon.click();
    }

}
