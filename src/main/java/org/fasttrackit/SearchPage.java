package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class SearchPage {
    private final SelenideElement searchKeyword = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final SelenideElement multipleSearchResults = $("[href='#/product/1']");
    private final SelenideElement searchResult = $(".card-link");

@Step("Click on the search field")
    public void clickOnTheSearchField(String text) {
        System.out.println("Click on the search field");
        this.searchKeyword.click();
        System.out.println("Type in " + text);
        searchKeyword.type(text);

    }
    @Step("Click on the search button")
    public void clickOnTheSearchButton() {
        System.out.println("Click on the search button");
        this.searchButton.click();
    }

    public String getSearchResult() {
       return this.searchResult.text();
    }

    public String getMultipleSearchResults(){
       return this.multipleSearchResults.text();
//        return $(".search-results").getText();
    }

}

