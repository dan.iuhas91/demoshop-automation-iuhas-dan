package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {

    private final SelenideElement cancelButton = $(".btn-danger");
    private final SelenideElement continueCheckoutButton = $(".btn-success");
    private final SelenideElement firstName = $("#first-name");
    private final SelenideElement lastName = $("#last-name");
    private final SelenideElement address = $("#address.form_input");
    private final SelenideElement checkoutPageTitle = $(".text-muted");
    private final SelenideElement errorElement = $(".error");

@Step("Click on the Cancel Button from Checkout Page")
    void clickOnTheCancelButton() {
        System.out.println("Click on the Cancel Button from Checkout Page");
        cancelButton.click();
    }
@Step("Click on the Continue Checkout Button")
    public void clickOnTheContinueCheckoutButton() {
        System.out.println("Click on the Continue Checkout Button");
        continueCheckoutButton.click();
    }
@Step("Click on the First Name Field")
    public void typeInFirstName(String text) {
        System.out.println("Click on the First Name Field");
        firstName.click();
        System.out.println("Type in " + text);
        firstName.type(text);
    }
    @Step("Click on the Last Name Field")
    public void typeInLastName(String text) {
        System.out.println("Click on the Last Name Field");
        lastName.click();
        System.out.println("Type in " + text);
        lastName.type(text);
    }
    @Step("Click on the Address Field")
    public void typeInAddress(String text) {
        System.out.println("Click on the Address Field");
        address.click();
        System.out.println("Type in " + text);
        address.type(text);
    }

    public String getCheckoutPageTitle() {
        return checkoutPageTitle.text();
    }
    public String getErrorMsg(){
        return errorElement.text();
    }
}
