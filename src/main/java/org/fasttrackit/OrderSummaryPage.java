package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummaryPage {
    private final SelenideElement pageTitle = $(".text-muted");
    private final SelenideElement cancelElement = $(".btn-danger");
    private final SelenideElement completeYourOrderElement = $(".btn-success");
    private final SelenideElement orderCompleteMsg = $(".text-center");

    public String getOrderSummaryPageTitle(){
        return pageTitle.text();
    }

    @Step("Click On The Cancel Button")
    public void clickOnTheCancelButton(){
        System.out.println("Click On The Cancel Button");
        cancelElement.click();
    }
    @Step("Click On The Complete your order Button")
    public void clickOnTheCompleteYourOrderButton(){
        System.out.println("Click On The Complete your order Button");
        completeYourOrderElement.click();
    }
    public String getOrderCompleteMsg(){
        return  orderCompleteMsg.text();
    }

}
