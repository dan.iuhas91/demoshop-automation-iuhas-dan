package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import net.bytebuddy.asm.Advice;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {

    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement logOutButton = $(".fa-sign-out-alt");
    private final SelenideElement errorMessage = $(".error");

    private final SelenideElement closeModal = $(".sr-only");


    @Step("Type in username")
    public void typeInUsername(String user) {
        System.out.println("Click on the Username Field");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }

    @Step("Type in password")
    public void typeInPassword(String pass) {
        System.out.println("Click on the Password field");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }

    @Step("Click on the login button")
    public void clickOnTheLoginButton() {

        System.out.println("Click on the Login Button");
        loginButton.click();
    }
    @Step("Click on the Log Out Button")
    public void clickOnTheLogOutButton() {
        System.out.println("Click on the Log Out Button");
        logOutButton.click();
    }

    public String errorMessageLogin() {
        return "The user has been locked out.";

    }

    public boolean isErrorMessageVisible() {
        return errorMessage.isDisplayed();
    }

    public void clickToCloseModal() {
        closeModal.click();
    }
    public String getErrorMsg(){
        return this.errorMessage.text();
    }



}

