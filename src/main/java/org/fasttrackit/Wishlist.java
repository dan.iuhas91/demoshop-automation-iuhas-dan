package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Wishlist {

    private SelenideElement wishlistCartIcon = $(".fa-cart-plus");
    private SelenideElement emptyWishlistPageElement = $("hr");
    private SelenideElement elementInWishlist = $(".card-link");


@Step("Click on the cart icon from the Wishlist page")
    public void clickOnTheWishlistProductCartIcon(){
        System.out.println("Click on the cart icon from the Wishlist page");
        wishlistCartIcon.click();
    }
    public String getEmptyWishlistPageText(){
        return this.emptyWishlistPageElement.text();
    }
    public boolean isEmptyWishlistMessageDisplayed(){
        return emptyWishlistPageElement.isDisplayed();
    }
    public String getNotEmptyWishlistMsg(){
        return elementInWishlist.text();

    }

}
