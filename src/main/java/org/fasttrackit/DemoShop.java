package org.fasttrackit;

import static com.codeborne.selenide.Selenide.$;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println("Demo Shop");
        System.out.println("User can login with valid credentials");
        Page page = new Page();
        page.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi Dino is displayed in the header: " + isLogged);
        System.out.println("Greeting msg is: " + greetingsMessage);

        System.out.println("-------------------------------------------");
        System.out.println("2. User can add product too cart from product cards.");
        page.openHomePage();
        ProductCards cards =  new ProductCards();
//        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
//        System.out.println("Product is : " + awesomeGraniteChips.getTitle());
//        awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println("-------------------------------------------");
        System.out.println("User can navigate to Home Page from Wishlist page");
        page.openHomePage();
        header.clickOnTheWishlistIcon();
        String pageTitle = page.getPageTtile();
        System.out.println(" Expected to be on the Wishlist page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTtile();
        System.out.println(" Expected to be on the Home page. Title is: " + pageTitle);

        System.out.println("-------------------------------------------");
        System.out.println("User can navigate to Home Page from Cart page");
        page.openHomePage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTtile();
        System.out.println(" Expected to be on the Cart page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTtile();
        System.out.println(" Expected to be on the Home page. Title is: " + pageTitle);


    }
}

//    private final String loginErrorMessage = $(".fa-exclamation-circle");
//User can navigate to Home Page from Wish-list page
//1.Open Home Page
//2. Click on the ,,favorites,, icon
//3. Click on the ,, Shopping bag,, icon
//Expected result - Home page is loaded


//-------------3
//Check the Wishlist functionality
//1. open the site https://fasttrackit-test.netlify.app/#/
//2. Login with a valid username and password
//3. Click on the heart icon of any product located below the price
//4. Click on the heart icon at the top right of the page



//-------------2
//- User can add product to cart from product cards
//1. open home page
//2. click on the "Awesome Granite Chips" cart icon
// Expected result - Mini cart icon shows 1 product in cart



//-------------1
//1. Open Home Page
//2.Click on the Login Button
//3.Click on Username Field
//4.Type in  dino
//5.Click on the pass  field
//6. Type in choochoo
//7. Click on the login button
