import io.qameta.allure.*;
import org.fasttrackit.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
@Epic("Cart Management")
@Feature("User journey to the cart management")
public class CartManagementTest {
    Page page = new Page();

    Header header = new Header();
    CartPage cartPage = new CartPage();
    ProductCards productCards = new ProductCards();
    ModalDialog modal = new ModalDialog();



    @BeforeClass
    public void setup() {
        page.openHomePage();

    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }
    @Description("Guest User navigate to cart page, empty cart message is displayed .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User navigate to cart page, empty cart message is displayed.")
    @Test
    public void when_user_navigate_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }
    @Description("Guest User can add one product to cart, and the empty cart message is not shown .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User can add one product to cart, and the empty cart message is not shown.")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());

    }
    @Description("Guest User can add two product to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User can add two product to cart.")
    @Test
    public void user_can_add_two_product_to_cart_from_product_cart() {
//        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2.");

    }
    @Description("Guest User can increment the amount of a product in cart page .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can increment the amount of a product in cart page.")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_increment_the_amount_of_a_product_in_cart_page(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.increaseAmount();
        assertEquals(item.getItemAmount(), "2");


    }
    @Description("Guest User can reduce the amount of a product in cart page .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can reduce the amount of a product in cart page.")
    @Test
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");

    }
    @Description("Valid User can reduce the amount of a product in cart page .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid user can reduce the amount of a product in cart page.")
    @Test(description = "reduce the amount of a product in cart page", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_reduce_the_amount_of_a_product_in_cart_page(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("2");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");

    }
    @Description("Guest User can delete a product in cart page by clicking trash button .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can delete a product in cart page by clicking trash button.")
    @Test
    public void user_can_delete_a_product_from_the_cart_page_by_clicking_the_trash_button() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheTrashCanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?", "The product is deleted from cart page");

    }
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void adding_one_product_to_cart_and_click_on_the_continue_shopping_button(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTtile(), "Products", "Expected to be on the Products page");

    }

}
