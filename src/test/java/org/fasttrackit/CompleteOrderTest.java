package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
@Epic("Complete a full Order")
@Feature("Valid user and Guest user can complete a order")
public class CompleteOrderTest {

    Page page = new Page();

    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    ModalDialog modal = new ModalDialog();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToReset();

    }

    @Description("Guest User can navigate to cart page from Order summary Page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User can navigate to cart page from Order summary Page by clicking red cancel button.")
    @Test(description = "Guest User can navigate to cart page from Order summary Page")
    public void user_can_navigate_to_cart_page_from_order_summary_page_clicking_cancel_button() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("Dan");
        checkoutPage.typeInLastName("Iuhas");
        checkoutPage.typeInAddress("Codru");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCancelButton();
        Assert.assertEquals(cartPage.getCartPageTitle(), "Your cart", "Expected to be on the Your cart page");

    }

    @Description("Guest User can complete a order.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Dan Iuhas")
    @Story("Guest User can complete a full order.")
    @Test(description = "Guest User can complete a order.")
    public void guest_user_can_complete_a_order() {
        Product product = new Product("3");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("Dan");
        checkoutPage.typeInLastName("Iuhas");
        checkoutPage.typeInAddress("Codru");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteYourOrderButton();
        Assert.assertEquals(orderSummaryPage.getOrderCompleteMsg(), "Thank you for your order!", "Expected to be on the Order complete page");

    }
    @Description("Valid User can complete a order.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Dan Iuhas")
    @Story("Valid User can complete a full order.")
    @Issue("DMS 002")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_complete_a_order(ValidAccount account) {
//        page.openHomePage();
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("Dan");
        checkoutPage.typeInLastName("Iuhas");
        checkoutPage.typeInAddress("Codru");
        checkoutPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteYourOrderButton();
        sleep(5 * 1000);
        Assert.assertEquals(orderSummaryPage.getOrderCompleteMsg(), "Thank you for your order!", "Expected to be on the Order complete page");

    }
}

