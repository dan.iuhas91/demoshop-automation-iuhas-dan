package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;
@Feature("User can add to wishlist")
@Epic("Wishlist ")
public class WishlistTest {

    Page page = new Page();
    Header header = new Header();
    Wishlist wishlist = new Wishlist();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToReset();

    }
    @Description("Guest User can navigate to Wishlist Page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can navigate to Wishlist Page")
    @Test(description = "Guest user navigate to Wishlist Page")
    public void user_can_navigate_to_wishlist_page() {
//        page.openHomePage();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTtile(), "Wishlist", "Expected to be on the Wishlist page");

    }
    @Description("Guest User can add a product to Wishlist.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can add a product to Wishlist")
    @Test(description = "User can add a product to Wishlist")
    public void user_can_add_product_to_wishlist_from_product_page() {
//        page.openHomePage();
        Product product = new Product("1");
        product.clickOnTheProductHeartIcon();
        header.clickOnTheWishlistIcon();
        sleep(5 * 1000);
        Assert.assertEquals(wishlist.getNotEmptyWishlistMsg(), "Awesome Granite Chips", "The product Awesome Granite Chips is added to Wishlist ");

        }
    @Description("Guest User can add multiple products to Wishlist.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can add multiple products to Wishlist")
    @Test(description = "User can add multiple products to Wishlist")
    public void user_can_add_two_product_to_wishlist_from_product_cart() {
//        page.openHomePage();
        Product product = new Product("1");
        product.clickOnTheProductHeartIcon();
        product.clickOnTheProductHeartIcon();
        header.clickOnTheWishlistIcon();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "Two product is added on the Wishlist page");

    }
    @Description("Guest User can add a product to Cart from Wishlist page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User can add a product to Cart from Wishlist page")
    @Test(description = "Guest User can add a product to Cart from Wishlist page")
    public void user_can_add_product_to_cart_from_wishlist_page() {
        Product product = new Product("9");
        product.clickOnTheProductHeartIcon();
        header.clickOnTheWishlistIcon();
        wishlist.clickOnTheWishlistProductCartIcon();
        header.getShoppingCartBadgeValue();
        assertEquals(header.getShoppingCartBadgeValue(), "1", "After adding one product to cart, from Wishlist page, badge shows 1");

       }
    @Description("Valid User can add a product to Wishlist.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can add a product to Wishlist")
    @Test(description = "Valid User can add a product to Wishlist",dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_add_product_to_wishlist(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("9");
        product.clickOnTheProductHeartIcon();
        header.clickOnTheWishlistIcon();
        wishlist.clickOnTheWishlistProductCartIcon();
        header.getShoppingCartBadgeValue();
        Assert.assertEquals(wishlist.getNotEmptyWishlistMsg(), "Awesome Granite Chips", "The product Awesome Granite Chips is added to Wishlist for all Valid Users ");

     }
}

