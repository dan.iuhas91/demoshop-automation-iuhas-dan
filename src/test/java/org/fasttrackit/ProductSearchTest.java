package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.atBottom;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

public class ProductSearchTest {
    Page page = new Page();

    Header header = new Header();
    CartPage cartPage = new CartPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();

    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();
//        header.clickOnTheShoppingBagIcon();
    }
    @Description("Guest User can search AGC product in search tab from the product page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search AGC product in search field")
    @Test(description = "User can search AGC product in search field")
    public void user_can_search_agc_product_in_search_field_from_the_product_page() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Awesome Granite Chips");
        searchPage.clickOnTheSearchButton();
        Assert.assertEquals(searchPage.getSearchResult(), "Awesome Granite Chips", "The product search is Awesome Granite Chips");


    }
    @Description("Guest User can search AMC product in search tab from the product page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search AMC product in search field")
    @Test( description = "User can search AMC product in search field")
    public void user_can_search_amc_product_in_search_field_from_the_product_page() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Awesome Metal Chair");
        searchPage.clickOnTheSearchButton();
        Assert.assertEquals(searchPage.getSearchResult(), "Awesome Metal Chair", "The product search is Awesome Metal Chair");

    }
    @Description("Guest User can search ASF product in search tab from the product page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search ASF product in search field")
    @Test( description = "User can search ASF product in search field")
    public void user_can_search_asf_product_in_search_field_from_the_product_page() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Awesome Soft Shirt");
        searchPage.clickOnTheSearchButton();
        Assert.assertEquals(searchPage.getSearchResult(), "Awesome Soft Shirt", "The product search is Awesome Soft Shirt");


    }
    @Description("Guest User can search GSP product in search tab from the product page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search GSP product in search field")
    @Test( description = "User can search GSP product in search field")
    public void user_can_search_gsp_product_in_search_field_from_the_product_page() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Gorgeous Soft Pizza");
        searchPage.clickOnTheSearchButton();
        Assert.assertEquals(searchPage.getSearchResult(), "Gorgeous Soft Pizza", "The product search is Gorgeous Soft Pizza");


    }
    @Description("Guest User can search ICH product in search tab from the product page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search ICH product in search field")
    @Test(description = "User can search ICH product in search field")
    public void user_can_search_ich_product_in_search_field_from_the_product_page() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Incredible Concrete Hat");
        searchPage.clickOnTheSearchButton();
        Assert.assertEquals(searchPage.getSearchResult(), "Incredible Concrete Hat", "The product search is Incredible Concrete Hat");

    }
    @Description("Guest User can search a product in search field and obtain multiple results.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can obtain multiple results")
    @Test(description = "User can obtain multiple results")
    public void user_can_search_a_product_in_search_field_and_obtain_multiple_results() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Awesome");
        searchPage.clickOnTheSearchButton();
        Assert.assertTrue(searchPage.getMultipleSearchResults().contains("Awesome"));

    }
    @Description("Guest User can search a product in search field that is not exist in product list.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("User can search a product that is not exist")
    @Test( description = "User can search a product that is not exist")
    public void user_search_a_product_in_search_field_that_is_not_on_the_list() {
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("DJ");
        searchPage.clickOnTheSearchButton();
        assertEquals(page.getPageTtile(), "Products", "Expected to be on the Empty Products page ");
    }

    @Description("Valid User can search a product in search field and obtain multiple results.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can obtain multiple results")
    @Test( description = "Valid User can obtain multiple results", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_search_a_product_in_search_field_that_is_not_on_the_list(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        SearchPage searchPage = new SearchPage();
        searchPage.clickOnTheSearchField("Awesome");
        searchPage.clickOnTheSearchButton();
        Assert.assertTrue(searchPage.getMultipleSearchResults().contains("Awesome"));
    }
}



