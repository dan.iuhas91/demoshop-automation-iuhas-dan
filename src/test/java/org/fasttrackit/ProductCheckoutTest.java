package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
@Epic("Checkout Page")
@Feature("User can navigate back and forward")
public class ProductCheckoutTest {
    Page page = new Page();

    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkoutPage = new CheckoutPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    ModalDialog modal = new ModalDialog();
    ProductCards productCards = new ProductCards();


    @BeforeClass
    public void setup() {
        page.openHomePage();
        header.clickOnTheBrandButton();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToReset();
    }

    @Description("Guest User can navigate to checkout page from cart page.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Dan Iuhas")
    @Story("User User can navigate to checkout page from cart page")
    @Test(description = "Guest User can navigate to checkout page from cart page", dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_navigate_to_checkout_page_from_cart_page(ProductData productData) {
        header.clickOnTheBrandButton();
        Product product = new Product(productData.getId());
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        final String checkoutPageTitle = checkoutPage.getCheckoutPageTitle();
        header.clickOnTheBrandButton();
        Assert.assertEquals(checkoutPageTitle, "Your information", "Checkout Page is open and Address information");


    }

    @Test()
    public void user_can_navigate_back_to_cart_page_from_checkout_page() {
        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.clickOnTheCancelButton();
        final String cartPageTitle = cartPage.getCartPageTitle();
        header.clickOnTheBrandButton();
        Assert.assertEquals(cartPageTitle, cartPage.getCartPageTitle(), "Cart page is open, and you see all the products there");

    }
    @Description("Guest User can navigate to order summery page from checkout if the first name is not fill.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can navigate to order summery page from checkout if the first name is not fill")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_not_navigate_to_order_summery_page_from_checkout_page_if_first_name_is_not_fill(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("");
        checkoutPage.typeInLastName("Iuhas");
        checkoutPage.typeInAddress("Galaxia Andromeda");
        checkoutPage.clickOnTheContinueCheckoutButton();
        final String errorMsg = checkoutPage.getErrorMsg();
        header.clickOnTheBrandButton();
        Assert.assertEquals(errorMsg, "First Name is required", "Error msg is displayed - First Name is required");

    }
    @Description("Guest User can navigate to order summery page from checkout if last name is not fill.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can navigate to order summery page from checkout if last name is not fill.")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_not_navigate_to_order_summery_page_from_checkout_page_if_last_name_is_not_fill(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("Dan");
        checkoutPage.typeInLastName("");
        checkoutPage.typeInAddress("In casuta cu alune");
        checkoutPage.clickOnTheContinueCheckoutButton();
        final String errorMsg = checkoutPage.getErrorMsg();
        header.clickOnTheBrandButton();
        Assert.assertEquals(errorMsg, "Last Name is required", "Error msg is displayed - Last Name is required");

    }
    @Description("Guest User can navigate to order summery page from checkout if the address is not fill.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can navigate to order summery page from checkout if the address is not fill.")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_not_navigate_to_order_summery_page_from_checkout_page_if_address_is_not_fill(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("DJ");
        checkoutPage.typeInLastName("Mixer");
        checkoutPage.typeInAddress("");
        checkoutPage.clickOnTheContinueCheckoutButton();
        final String errorMsg = checkoutPage.getErrorMsg();
        header.clickOnTheBrandButton();
        Assert.assertEquals(errorMsg, "Address is required", "Error msg is displayed - Address is required");

    }
    @Description("Guest User can navigate to order summery page from product page.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Dan Iuhas")
    @Story("User can navigate to order summery page from product page.")
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    public void user_can_navigate_to_order_summery_page_from_product_page(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkoutPage.typeInFirstName("Dan");
        checkoutPage.typeInLastName("Iuhas");
        checkoutPage.typeInAddress("Codru");
        checkoutPage.clickOnTheContinueCheckoutButton();
        final String orderSummaryPageTitle = orderSummaryPage.getOrderSummaryPageTitle();
        header.clickOnTheBrandButton();
        Assert.assertEquals(orderSummaryPageTitle, "Order summary", "Expected to be on the Order summary page");

    }
}
