package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Login")
@Epic("User journey though  demo shop")
public class DemoShopTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToReset();
    }

    @Test(description = "User turtle can login with valid credentials")
    @Description("User turtle can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Lead("Dan Iuhas")
    @Link(name = "FasttrackIt Demo Shop", url = "")
    @Issue("DMS-001")
    @Story("Login with valid credentials")
    @Ignore("Already tested")
    public void user_turtle_can_login_with_valid_credentials() {
//        page.openHomePage();
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "Logged in with turtle, expected greeting message to be Hi turtle!");
    }

    @Description("Valid User can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Lead("Dan Iuhas")
    @Link(name = "FasttrackIt Demo Shop", url = "")
    @Issue("DMS-001")
    @Story("Login with valid credentials")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_login_with_valid_credentials(ValidAccount account) {
//        page.openHomePage();
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Logged in with valid user, expected greeting message to be:" + account.getGreetingsMsg());
    }

    @Description("Valid User can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Ignore("Already tested")
    @Test
    public void user_beetle_can_login_with_valid_credentials() {
//        page.openHomePage();
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), "Hi beetle!", "Logged in with beetle, expected greeting message to be Hi beetle!");
    }

    @Description("User can navigate to Cart Page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("A guest, or valid user can navigate to Cart Page")
    @Test
    public void user_can_navigate_to_cart_page() {
//        page.openHomePage();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTtile(), "Your cart", "Expected to be on the Cart Page");
    }

    @Description("User can navigate to Home Page from Wishlist Page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("A guest, or valid user can navigate back to Home Page from Wishlist Page.")
    @Test
    public void user_can_navigate_to_home_page_from_wishlist_page() {
//        page.openHomePage();
        header.clickOnTheWishlistIcon();
        assertEquals(page.getPageTtile(), "Wishlist", "Expected to be on the Wishlist page");
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTtile(), "Products", "Expected to be on the Products page");
    }

    @Description("User can navigate to Home Page from Cart Page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("A guest, or valid user can navigate to Home Page from Cart Page.")
    @Test
    public void user_can_navigate_to_homepage_from_cart_page() {
//        page.openHomePage();
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTtile(), "Your cart", "Expected to be on the Cart Page");
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTtile(), "Products", "Expected to be on the Products page");

    }

    @Description("User can add product to cart from Product Cart.")
    @Severity(SeverityLevel.BLOCKER)
    @Owner("Dan Iuhas")
    @Story("A guest, or valid user can add product to cart from Product Cart.")
    @Test
    public void user_can_add_product_to_cart_from_product_cart() {
//        page.openHomePage();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");

    }

    @Description("User can click on the product name and open a detailed page.")
    @Severity(SeverityLevel.NORMAL)
    @Owner("Dan Iuhas")
    @Story("user can click on the product name and open a detailed page about him")
    @Test
    public void user_can_click_on_the_product_name_and_and_open_a_detailed_page() {
        Product product = new Product("1");
        product.clickOnTheProductNameLink();
        assertEquals(product.getProductNameDisplayed(), "Awesome Granite Chips", "Show more detailed about product");
    }
    @Description("No valid login with invalid credentials or ban users.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("user can not login with invalid credentials, or if is ban from account")
    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthentificationDataProvider.class, description = "User turtle can login")
    public void non_valid_login_with_invalid_credentials(InvalidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.isErrorMessageVisible();
        String errorMsg = modal.getErrorMsg();
        assertTrue(errorMessageVisible, "Error message is displayed");
        assertEquals(errorMsg, account.getErrorMsg());
//        (Ctrl + Alt + V extract variable errorMessageVisible)
    }

}


