package org.fasttrackit;

import com.codeborne.selenide.Selenide;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Epic("Detailed Product Page")
@Feature("User can open a detailed product page")
public class DetailedPageTest {
    Page page = new Page();

    Header header = new Header();

    ModalDialog modal = new ModalDialog();
    ProductDetailPage productDetailPage = new ProductDetailPage();
    Wishlist wishlist = new Wishlist();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickToReset();
    }
    @Description("Guest User can navigate to detailed product page and add to cart a item.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Guest User can navigate to detailed product page.")
    @Test(description = "Guest User can navigate to detailed product page and add to cart a item")
    public void user_can_navigate_to_detailed_product_page_and_add_to_cart_a_item() {

        Product product = new Product("1");
        product.clickOnTheProductNameLink();
        productDetailPage.clickOnTheDetailedPageCartIcon();
        assertTrue(header.isShoopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
        sleep(5 * 1000);


    }
    @Description("Valid User can navigate to detailed product page and add to cart a item.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can navigate to detailed product page.")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_navigate_to_detailed_product_page_and_add_to_cart_a_item(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductNameLink();
        productDetailPage.clickOnTheDetailedPageCartIcon();
        assertTrue(header.isShoopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");

    }
    @Description("Valid User can navigate to detailed product page and add to wishlist a item.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can navigate to detailed product page and add to wishlist.")
    @Test(dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_navigate_to_detailed_product_page_and_add_to_wishlist_a_item(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductNameLink();
        productDetailPage.clickOnTheDetailedProductPageHeartIcon();
        assertTrue(header.isShoopingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to wishlist, badge shows 1.");
    }
}