package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthentificationDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
@Epic("Sorting")
@Feature("Valid User and Guest user can sort products by name and price")
public class ProductSortingTest {
    Page page = new Page();
    ProductCards productList = new ProductCards();
    ModalDialog modal = new ModalDialog();
    Header header = new Header();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();
    }

    @Description("User can sort products A to Z .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can sort products A to Z.")
    @Test(description = "Guest User can sort products A to Z")
    public void when_sorting_products_a_to_z_products_are_sorted_alphabetically() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
    }

    @Description("User can sort products Z to A .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can sort products Z to A.")
    @Test(description = "Guest User can sort products Z to A")
    public void when_sorting_products_Z_to_A_products_are_sorted_alphabetically_DESC() {
        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheZASortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());

    }

    @Description("Guest User can sort products by price low to high .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can sort products by price low to high.")
    @Test(description = "Guest User can sort products by price low to high")
    public void when_sorting_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());

    }

    @Description("Guest User can sort products by price high to low .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("User can sort products by price high to low.")
    @Test
    public void when_sorting_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low() {
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());

    }

    @Description("Valid User can sort product by price high to low.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Issue("Bug with Beetle user")
    @Story("Valid User can sort product by price high to low. User Beetle has a Bug")
    @Test(description = "Valid user can sort product by price high to low", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_sort_products_by_price_high_to_low_products_are_sorted_by_price_high_to_low(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }
    @Description("Valid User can sort product by price low to high.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Issue("Bug with Beetle user")
    @Story("Valid User can sort product by price low to high. User Beetle has a Bug")
    @Test(description = "Valid user can sort product by price low to high", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class)
    public void valid_user_can_sort_products_by_price_low_to_high_products_are_sorted_by_price_low_to_high(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceHiLo();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
    }
    @Description("Valid User can sort product by Z to A .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can sort products by Z to A.")
    @Test(description = "Valid User can sort products Z to A", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class )
    public void valid_user_can_sort_products_Z_to_A_products_are_sorted_alphabetically_DESC(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();

        Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheSortByPriceLoHi();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getPrice(), firstProductBeforeSort.getPrice());
        assertEquals(lastProductAfterSort.getPrice(), lastProductBeforeSort.getPrice());

    }
    @Description("Valid User can sort product by A to Z .")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Dan Iuhas")
    @Story("Valid User can sort products by A to Z.")
    @Test(description = "Valid User can sort products A to Z", dataProvider = "validCredentials", dataProviderClass = AuthentificationDataProvider.class )
    public void valid_user_can_sort_products_a_to_z_products_are_sorted_alphabetically(ValidAccount account) {

       Product firstProductBeforeSort = productList.getFirstProductInList();
        Product lastProductBeforeSort = productList.getLastProductInList();
        productList.clickOnTheSortButton();
        productList.clickOnTheAZSortButton();
        Product firstProductAfterSort = productList.getFirstProductInList();
        Product lastProductAfterSort = productList.getLastProductInList();

        assertEquals(firstProductAfterSort.getTitle(), firstProductBeforeSort.getTitle());
        assertEquals(lastProductAfterSort.getTitle(), lastProductBeforeSort.getTitle());

    }
}
