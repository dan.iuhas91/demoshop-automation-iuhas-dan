package org.fasttrackit;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
@Epic("Log-out")
@Feature("Log-out from account")
public class LoginTest {

    Page page = new Page();
    ModalDialog modal = new ModalDialog();
    Header header = new Header();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        Footer footer = new Footer();
        footer.clickToReset();

    }

    @Test
    public void user_dino_can_log_out_from_the_account() {

        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogOutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Hello Guest! is the logout message");
        sleep(5 * 1000);
    }

    @Test
    public void user_turtle_can_log_out_from_the_account() {

        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogOutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Hello Guest! is the logout message");
        sleep(5 * 1000);
    }

    @Test
    public void user_beetle_can_log_out_from_the_account() {

        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        modal.clickOnTheLogOutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Hello Guest! is the logout message");
        sleep(5 * 1000);
    }


}
