package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.fasttrackit.InvalidAccount;
import org.fasttrackit.ValidAccount;
import org.testng.annotations.DataProvider;

public class AuthentificationDataProvider {

    @DataProvider(name = "validCredentials")
    public Object[][] getCredentials() {
        ValidAccount dino = new ValidAccount("dino", "choochoo");
        ValidAccount turtle = new ValidAccount("turtle", "choochoo");
        ValidAccount beetle = new ValidAccount("beetle", "choochoo");
        return new Object[][]{
                {dino},
                {turtle},
                {beetle}

        };

    }

    @DataProvider(name = "invalidCredentials")
    public Object[][] getInvalidCredentials() {
        InvalidAccount locked = new InvalidAccount("locked", "choochoo", "The user has been locked out.");
        InvalidAccount unknown = new InvalidAccount("unknown", "choochoo", "Incorrect username or password!");
        InvalidAccount wrongPass = new InvalidAccount("beetle", "wrongpassword", "Incorrect username or password!");
        InvalidAccount noPassword = new InvalidAccount("turtle", "", "Please fill in the password!");
        return new Object[][]{
                {locked},
                {unknown},
                {wrongPass},
                {noPassword},

        };
    }
}
